document.addEventListener("DOMContentLoaded", function(){
  console.log("Here we are!");
  
  // "nav-menu" is the class associated with the ul that contains
  // the li's that make up the navigation menu.
  let navMenu = document.getElementsByClassName("nav-menu");

  // "nav-toggle" is the id of the checkbox element that's associated
  // with the hamburger menu icon.  The icon is the label for the
  // checkbox.  The checkbox is acually hidden.
  // Here we select all of the link elements that comprise the 
  // navigation menu and assign to them a click handler wherein
  // we "click" the nav-toggle checkbox.  The ultimate effect is
  // to cause the nav menu to slide off to the right when one 
  // of the navigation links is clicked.
  navMenu[0].addEventListener("click", function(el) {
    if(el.target && el.target.nodeName == "A") {            
      document.getElementById("nav-toggle").click();
      // console.log(el.target.innerHTML);
    }
  });
}, false)
// End ocument.addEventListener("DOMContentLoaded", function(){})